# Rename TV Show files
This script downloads episode names from http://thetvdb.com and adds them as a suffix to the appropriate files

For this to work your file needs to contain the season and episode number (in accordance to thetvdb) in this format: S#E# (doesn't matter if lower or upper case)

The script only supports the aired order so far

## Usage
Enter the directory in which the files you want to rename are (you can have subdirectories with different seasons, just not different shows) and call the script

Arguments:

`-s, --show` - Name of the show

`-n, --season` - Seasons to be renamed (numbers seperated by space or 'all' for all seasons in the directory and sub directories)

`-sp, --show-path` - Path to the show's directory

`-c, --correct-path` - Don't ask if path is correct

`-t, --trust` - Don't prompt before changing file names

`-x, --linux` - Don't replace characters that are ilegal in NTFS

`-l, --lang` - Language of the episode titles

`-p, --print_langs` - Print available languages
