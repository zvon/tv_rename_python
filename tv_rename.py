#!/usr/bin/python3

import re
import pathlib
import urllib.request
import argparse


opener = urllib.request.FancyURLopener({})


def get_def_url(show):
    show_search = re.sub(' ','+',show)
    url = 'https://www.thetvdb.com/search?q=' + show_search + '&l=' + language
    web_page = opener.open(url)
    basic = str(web_page.read().decode('utf-8'))
    rough = re.findall('<td><a href=\"/series.*?</td>', basic, re.I)
    for i in range(len(rough)):
        text = re.sub('.*series.*?>', '', rough[i])[:-9]
        if text:
            print('{}. {}'.format(i+1, text))
    which = int(input('Which TV Show is the right one? ')) - 1
    url = "https://www.thetvdb.com" + re.sub('\">.*', '', rough[which][13:])
    return url


def one_season(ren_dir, show, season_num, url='', trust=False):
    if url == '':
        url = get_def_url(show)
    url += "/seasons/" + str(season_num)
    web_page = opener.open(url)
    season = str(web_page.read().decode('utf-8'))
    season = re.search('<table class=.*?id=\"translations\">.*</table>', season, re.DOTALL)
    if season:
        season = season.group()
    else:
        return
    season = re.findall('<tr>.*?</tr>', season.replace('\n','').replace('\r',''))[1:]
    episodes = []
    for line in season:
        episode = re.findall('<td>.*?</td>', line)[1]
        ep = re.findall('<span.*?</span>', episode)
        episode = ''
        for lan in ep:
            if re.search('<span.*language=\"' + language + '\".*', lan):
                episode = lan
                break;
        if len(episode) == 0:
            episode = ep[0]
        episode = re.sub('\s*</span>\s*', '', re.sub('\s*<span.*?>\s*', '', episode))
        episodes.append(episode)
    path_dir = pathlib.Path(ren_dir)
    ren_eps = []
    fin_eps = []
    sorted_eps = []
    i = 0
    ren_eps = []
    while(len(ren_eps) == 0 and i <= 1000):
        zeros = i*'0'
        ren_eps = list(path_dir.glob('**/*[sS]'+zeros+season_num+'[eE]*'))
        i += 1
    path = ''
    i = 0
    while i < len(ren_eps):
        name = ren_eps[i].name
        path = str(ren_eps[i].absolute())
        path = re.sub(name, '', path)
        num = re.sub('.*[sS][0-9]{1,2000}[eE]', '', name)
        num = int(re.search('[0-9]{1,2000}', num).group()) - 1
        if num < len(episodes):
            name = re.sub('\..*', '', name) + ' - ' + episodes[num] + re.sub('.*\.', '.', name)
            name = re.sub('\/','',name)
            if not linux:
                name = re.sub('\?','',name)
                name = re.sub('<','is less than',name)
                name = re.sub('>','is more than',name)
                name = re.sub(':',' -',name);
                name = re.sub('"','',name);
                name = re.sub('\\\\','',name);
                name = re.sub('|','',name);
                name = re.sub('\*','',name);
            fin_eps.append(name)
            sorted_eps.append(name)
            i += 1
        else:
            del ren_eps[i]
    sorted_eps.sort()
    for ep in sorted_eps:
        print(ep)
    do_it = ''
    if trust:
        do_it = 'y'
    while do_it == '':
        do_it = input('Does this seem ok? (y/n) ')
    do_it = do_it.lower()[0]
    if do_it == 'y':
        for i in range(len(ren_eps)):
            ren_eps[i].rename(path + fin_eps[i])


def multi_season(ren_dir, show, which, trust=False):
    url = get_def_url(show)
    if which[0] != 'all':
        for i in which:
            if i != '' and len(re.findall('[0-9]', i)) == len(i):
                one_season(ren_dir, show, i, url)
    else:
        path_dir = pathlib.Path(ren_dir)
        ren_eps = path_dir.glob('**/*[sS]*[eE]*')
        season = []
        for ep in ren_eps:
            seas = re.search('[sS][0-9]{1,2000}[eE][0-9]{1,2000}', ep.name)
            if seas:
                seas = seas.group()
                seas = re.search('[sS][0-9]{1,2000}', seas)
                if seas:
                    seas = seas.group()
                    seas = re.sub('[sS]', '', seas)
                    if seas not in season:
                        season.append(seas)
        season.sort()
        season = [str(int(x)) for x in season]
        for i in season:
            one_season(ren_dir, show, i, url, trust)


ren_dir = pathlib.Path('.')
languages_words = {'en': 'English', 'sv': 'Svenska', 'no': 'Norsk', 'da': 'Dansk', 'fi': 'Suomeksi', 'nl': 'Nederlands', 'de': 'Deutsch', 'it': 'Italiano', 'es': 'Español', 'fr': 'Français', 'pl': 'Polski', 'hu': 'Magyar', 'el': 'Greek', 'tr': 'Turkish', 'ru': 'Russian', 'he': 'Hebrew', 'ja': 'Japanese', 'pt': 'Portuguese', 'zh': 'Chinese', 'cs': 'Czech', 'sl': 'Slovenian', 'hr': 'Croatian', 'ko':'Korea'}

parser = argparse.ArgumentParser(description='Rename TV episodes.')
parser.add_argument('--show', '-s', dest='show', metavar='Show_name', type=str, default='_', help='TV Show from which you want episode names (needs to be in quotation if more than one word)')
parser.add_argument('--season', '-n', dest='season', metavar='Season_number', type=str, default='_', help='Season number/s (if multiple in quotation and seperated by one space) or \'all\' for all seasons in this folder (if not provided, \'all\' is assumed')
parser.add_argument('--correct-path', '-c', dest='cor_path', action='store_const', const=True, default=False, help='This is the correct path, stop asking me!')
parser.add_argument('--show-path', '-sp', dest='show_path', metavar='Show-path', type=str, default='.', help='Path of the show')
parser.add_argument('--trust', '-t', dest='trust', action='store_const', const=True, default=False, help='Don\'t ask whether the names are correct')
parser.add_argument('--linux', '-x', dest='linux', action='store_const', const=True, default=False, help='Don\'t replace characters in file names that are ilegal in Windows')
parser.add_argument('--lang', '-l', dest='lang', metavar='Language', type=str, default='en', help='Select which language the episode names should be in')
parser.add_argument('--print_langs', '-p', dest='print', action='store_const', const=True, default=False, help='Print available languages')
args = parser.parse_args()

show = args.show
season = args.season
trust = args.trust
linux = args.linux

if args.show_path != ".":
	args.cor_path = True

if args.print:
    for i in languages_words:
        print(i + " - " + languages_words[i])
    exit()

if args.lang in languages_words:
    language = args.lang
else:
    print('Invalid language choice')
    exit()

if args.show_path[0] == '/' or args.show_path[0] == '.':
    ren_dir = pathlib.Path(args.show_path)
else:
    ren_dir = pathlib.Path('./'+args.show_path)

change_dir = 'y'

if not args.cor_path:
    change_dir = input('Is this the right directory: \'{}\' ? (y/n) '.format(ren_dir.absolute())).lower()[0]

while change_dir != 'y':
    ren_dir = pathlib.Path(input('Insert correct path: '))
    if not ren_dir.exists():
        print('Unfortunatelly this is not a directory on your computer. Try again')
    else:
        change_dir = input('Is this the right directory: \'{}\' ? (y/n) '.format(ren_dir.absolute())).lower()[0]

while not ren_dir.exists():
    print('Unfortunatelly this is not a directory on your computer. Try again')
    ren_dir = pathlib.Path(input('Insert correct path: '))

if show == '_':
    show = ren_dir.absolute().name
    if input('Is this the show name: ' + show + '? (y/n) ').lower()[0] == 'n':
        show = input('Pleas insert name of the TV Show: ')
if season == '_':
    season = 'all'
season = season.lower()
split_season = season.split(' ')
while len([x for x in split_season if x.isdigit()]) != len(split_season) and season[0] != 'q' and season != "all":
    season = input('Please only insert numbers. Try again: (type \'q\' to exit)\n')
    split_season = season.split(' ')
dups = []
for x in split_season:
    if x in dups:
        print("You have entered a duplicate season {}, the duplicate was removed".format(x))
    else:
        dups.append(x)
split_season = dups
split_season.sort()
if season[0] == 'q':
    exit()
if split_season[0] != 'all' and len(split_season) == 1:
    one_season(str(ren_dir.absolute()), show, season, '', trust)
else:
    multi_season(str(ren_dir.absolute()), show, split_season, trust)
